def call(name){
   pipeline{
    agent any
    stages{
        stage('Echo'){
            steps{
                echo "Hey, ${name}, how are you? First try with shared libraries example."
            }
        }
        stage('Bye'){
            steps{
                echo "Good bye!, ${name}!"
            }
        }
    }
    
} 
}

